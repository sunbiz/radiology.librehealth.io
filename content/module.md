+++
title = "Radiology Module"
+++

## Vision
The OpenMRS radiology model aims at adding capabitlities of a Radiology Information system (RIS)onto OpenMRS.

## Objectives
Develop a module capable of manage infomation in a radiology department.
1)Identify radiology's department workflow according to radiology experts.
2)Design an architecture that supports basic radiology's department services.
3)Implement the module as suggested by architecture, by using  free and open source software.
4)Satisfy modules' quality parameters set by community.

## Features
Current set of features include placing radiology orders in openmrs, configure a set of concept classes to define the orderable imaging procedure at the healthacre facility, configure DICOM web viewer like oviyam or weasis to open the ordered radiology studies from within OpenMRS and create radiology report once a radiology order is completed.

## Limitations
The module has not yet been officially released. We are working very hard on first release.
